fuzzycreator
===========

What is it?
-----------
fuzzycreator is a toolkit for automatic generation and analysis of fuzzy sets
from data. It facilitates the creation of both conventional and
non-conventional (non-normal and non-convex) type-1, interval type-2 and
general type-2 (zSlices-based) fuzzy sets from data. These FSs may then
be analysed and compared through a series of tools and measures (included
in the toolkit), such as evaluating their similarity and distance.
For a list of available measures, please refer to docs/measures.pdf.


Referencing
-----------
You are free to use the toolkit in accordance with the license detailed below.
If you use the toolkit in your own publications, please cite the following paper:
    McCulloch, Josie "fuzzycreator: A Python-Based Toolkit for Automatically Generating and Analysing Data-Driven Fuzzy Sets" Fuzzy Systems (FUZZ-IEEE), 2017 IEEE International Conference on. IEEE, 2017.
You can find a copy of the paper at http://eprints.nottingham.ac.uk/44376/


The Latest Version
------------------
The latest version can found at
https://bitbucket.org/JosieMcCulloch/fuzzycreator


Documentation
-------------
API and a list of included measures can be found within docs/.
For example code of how to use the toolkit, see examples/.


Prerequisites
--------------
* python 2.7 or python 3
* scipy
* numpy
* matplotlib


Installation
------------
python setup.py install


Contacts
--------
If you wish to contact about new releases, feature requests or submit bugs,
please contact the mail address: j.mcculloch@leeds.ac.uk.


License
-------
Copyright (C) 2016 Josie McCulloch

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
